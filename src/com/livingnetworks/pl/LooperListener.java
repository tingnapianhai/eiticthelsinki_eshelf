package com.livingnetworks.pl;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class LooperListener implements OSCListener {
	
	//private Activity cont;
	private JSONObject list;
	private Handler handler;

	public LooperListener(JSONObject obj, Handler han) {
		list = obj;
		this.handler = han;
		//cont = context;
	}

	@Override
	public void acceptMessage(Date arg0, OSCMessage msg) {
		
		String address = msg.getAddress();
		
		//poster - command, playvideo
		if(address.equals("/playvideo")) {
			try {
				//if (list.getString("postertype").equals("demotest")) {
					Message message = new Message();
					MSLConfig.OscMsgFromOSCServer = msg.getArguments()[0].toString();
					message.what = GantLondon0.MSG_MSL_OSC_ResponseFromOSCServer;
					message.obj = list.getString(msg.getArguments()[0].toString());
					this.handler.sendMessage(message);
					Log.v("test", "osc cmd back - contentid:/" + msg.getArguments()[0].toString()+"/");
				}
			catch (JSONException e) {	e.printStackTrace(); }
		}else if(address.equals("/full")) {
		}
	}

}

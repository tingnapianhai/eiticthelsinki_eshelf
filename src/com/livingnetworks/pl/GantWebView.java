package com.livingnetworks.pl;

import com.livingnetworks.demo2.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.os.Vibrator;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;

@SuppressLint({ "NewApi", "SetJavaScriptEnabled" })
public class GantWebView extends Activity implements View.OnClickListener{

	private final String TAG = "GantWebView";
	
	//private LinearLayout midsection;// dynamically change the mid section
	WebView wv;
	
	private String weburl = "http://www.gant.co.uk/";
	
	private ImageButton superback;
	
	private Vibrator vi;
	
	private Handler webview_handler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what) {
			case 1:
				//midsection.removeAllViews();
				
				WebSettings webs = wv.getSettings();
				webs.setJavaScriptEnabled(true);
				
				//使webview全屏,不需要再scrollView...
				webs.setUseWideViewPort(true);
				webs.setLoadWithOverviewMode(true);
				webs.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
				
				//webs.setRenderPriority(RenderPriority.HIGH);
				
				wv.setWebChromeClient(new WebChromeClient() {
					@Override
					public void onProgressChanged(WebView view, int progress) {
							//midsection.removeAllViews();
							//midsection.addView(wv, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
					} }  );
				
				wv.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view,String url) {
						view.loadUrl(url);
						return true;
					}
					@Override
					public void onReceivedError(WebView view, int errorCode,
							String description, String failingUrl) {
					}
				});
				wv.loadUrl((String) msg.obj);
				
				/*WebBackForwardList mWebBackForwardList = wv.copyBackForwardList();
				Log.v(TAG, "web history: " + mWebBackForwardList.getCurrentIndex());*/
				
				break;
			default:break;
			}
			return false;
			}
		});

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gantwebview);
		
		vi = (Vibrator)this.getSystemService(VIBRATOR_SERVICE);
		vi.vibrate(100);
		
		//midsection = (LinearLayout) findViewById(R.id.gantwebview_midsection);

		superback = (ImageButton) findViewById(R.id.gantwebview_superback);
		superback.setOnClickListener(this);
		
		//wv = new WebView(this);
		wv = (WebView) findViewById(R.id.gantwebview_wv);
		weburl = this.getIntent().getExtras().getString("weburl");
		//midsection.addView(wv, new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
		Message msg = new Message();
		msg.what = 1;
		msg.obj = weburl;
		webview_handler.sendMessage(msg);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
	} // onCreate
	
	@Override
	protected void onResume() {
		MSLConfig.removeNavBar(this);
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		if(wv!=null){
			wv.destroy();
			wv = null;
		}
		//midsection.removeAllViews();
		
		MSLConfig.removeNavBar(this);
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if(wv!=null){
			wv.destroy();
			wv = null;
		}
		//midsection.removeAllViews();
		
		System.gc();
		Runtime.getRuntime().gc();
		this.setResult(GantLondon0.ACTIVITYRESULT_REQUESTCODE_WEBVIEW);
		super.onDestroy();
	}
	
	@Override
	public void onLowMemory() {
		System.gc();
		Runtime.getRuntime().gc();
	}

	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.gantwebview_superback) {
			setResult(GantLondon0.ACTIVITYRESULT_REQUESTCODE_WEBVIEW);
			finish();
		}
	};

}

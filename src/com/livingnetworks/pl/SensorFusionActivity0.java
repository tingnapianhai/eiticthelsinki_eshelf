/************************************************************************************
 * Copyright (c) 2012 Paul Lawitzki
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 ************************************************************************************/

package com.livingnetworks.pl;

import java.io.IOException;
import java.math.RoundingMode;
import java.net.InetSocketAddress;
import java.text.DecimalFormat;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortOut;


public class SensorFusionActivity0 implements SensorEventListener {
	
	/**
	 * 2014-10-31
	 * for shelf visual-search
	 */
	private static boolean INBORDER = false;
	private int recorder = 0;//2014-10-31 记录测direction的次数
	
	/** 2014-10-09
	 * for pointer-4-pieces
	 */
	public static boolean ifPointer4PiecesCalibrating = false;//true: calibrate now, sending 4corner; false, not calibrate now, sending normal one;
	public float Acceleration_Y = 0.0f; //used to judge if phone is 竖立还是横放，若
	public float Acceleration_Y_filter = 0.0f;
	public float Acceleration_Z = 0.0f;
	public float Acceleration_Y_pre = 0.0f;
	public float Acceleration_Z_pre = 0.0f;
	
	static int screenImageNr = 0;//0 is poster, 2 is mannequin
	
	//只用于临时更新主UI的background-image
	private static float x_pointer_pre = 0f;
	
	/**
	 * self-define, for gant-london-real 2014-04-17
	 */
	OSCMessage msg = null;
	OSCPortOut sender;
	OSCPortOut sender_TV;
	static boolean ifSendOSCPositionMessage = true;//初始化时要保证normal状态,即 可以发送OSC message
	static boolean ifKeepingXYPointerStill = false;//初始化时要保证normal状态,即 x&y pointer要实时更新,不应该是still
	//boolean ifServerIP_TV = false;
	
	private static SensorManager mSensorManager = null;
    private float[] gyro = new float[3];
    private float[] gyroMatrix = new float[9];
    private float[] gyroOrientation = new float[3];
	int angle_x = 0;
	int angle_y = 0;
    private float[] magnet = new float[3];
    private float[] accel = new float[3];
    private float[] accMagOrientation = new float[3];
    private float[] fusedOrientation = new float[3];
    private float[] rotationMatrix = new float[9];
    public static final float EPSILON = 0.000000001f;
    private static final float NS2S = 1.0f / 1000000000.0f;
	private long timestamp; //TODO lang ?
	private boolean initState = true;
	private static float angle[] = { 0.0f, 0.0f, 0.0f };
	private static float angle_pointer[] = { 0.0f, 0.0f, 0.0f };
	public static final int TIME_CONSTANT = 30;
	public static final float FILTER_COEFFICIENT = 0.7f;
	//private Timer fuseTimer = new Timer();
	private static float x_pointer = 0;
	private static float y_pointer = 0;
	private float x_pointer_still = 0;
	private float y_pointer_still = 0;
	
	private static float pointerShiftX = -2.6f; //-7.333f start-pointer for poster, 0.0f start-pointer for mannequin
	private static float pointerShiftY = 1.5f;
	private static float pointerMouseReferX = 0.0f;//像鼠标一样设置border
	private static float pointerMouseReferY = 0.0f;
	
	private float pointerShiftX_pointer4pieces = 0f;
	private float pointerShiftY_pointer4pieces = 0f;
	calculateFusedOrientationTask task;
	
	private static int X_Pointer_index = 2;//used to be 2...
	private static int Y_Pointer_index = 0;
	
	// The following members are only for displaying the sensor output.
	//public Handler mHandler;
	DecimalFormat d = new DecimalFormat("#.##");
	
    public SensorFusionActivity0(SensorManager sm, OSCPortOut oscClient, OSCPortOut oscClientTV) {
    	recorder = 0;//2014-10-31 记录测direction的次数
    	/**
    	 * self-define, for gant-london-real 2014-04-17
    	 */
    	sender = oscClient;
    	this.sender_TV = oscClientTV;
    	//sender.connect(new InetSocketAddress(MSLConfig.server_ip, MSLConfig.server_port));
    	//sender_TV.connect(new InetSocketAddress(MSLConfig.server_ip_tv, MSLConfig.server_port));
    	//ifSendOSCPositionMessage = true;//初始化时要保证normal状态,即 可以发送OSC message
    	//ifKeepingXYPointerStill = false;//初始化时要保证normal状态,即 x&y pointer要实时更新,不应该是still
    	
        gyroOrientation[0] = 0.0f;
        gyroOrientation[1] = 0.0f;
        gyroOrientation[2] = 0.0f;
 
        // initialise gyroMatrix with identity matrix
        gyroMatrix[0] = 1.0f; gyroMatrix[1] = 0.0f; gyroMatrix[2] = 0.0f;
        gyroMatrix[3] = 0.0f; gyroMatrix[4] = 1.0f; gyroMatrix[5] = 0.0f;
        gyroMatrix[6] = 0.0f; gyroMatrix[7] = 0.0f; gyroMatrix[8] = 1.0f;
 
        // get sensorManager and initialize sensor listeners
        mSensorManager = sm;
        task  = new calculateFusedOrientationTask();
        initListeners();
        
        // wait for one second until gyroscope and magnetometer/accelerometer
        // data is initialised then scedule the complementary filter task
        //fuseTimer.scheduleAtFixedRate(new calculateFusedOrientationTask(),1000, TIME_CONSTANT);
        
        //mHandler = new Handler();
        d.setRoundingMode(RoundingMode.HALF_UP);
        d.setMaximumFractionDigits(3);
        d.setMinimumFractionDigits(3);
    }
    
    public void Stop() {
    	// unregister sensor listeners to prevent the activity from draining the device's battery.
    	mSensorManager.unregisterListener(this);
    	Log.v("test", "inside stoip");
    	//fuseTimer.cancel();
    }
    
    // This function registers sensor listeners for the accelerometer, magnetometer and gyroscope.
    public void initListeners(){
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),SensorManager.SENSOR_DELAY_GAME);
        //mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),SensorManager.SENSOR_DELAY_GAME);
        //mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),SensorManager.SENSOR_DELAY_GAME);
    }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		switch(event.sensor.getType()) {
	    case Sensor.TYPE_ACCELEROMETER:
	        // copy new accelerometer data into accel array and calculate orientation
	        System.arraycopy(event.values, 0, accel, 0, 3);//Do Not Delete
	        calculateAccMagOrientation();//Do Not Delete
	        Vuforia_recognition(event);
	        break;
	 
	    case Sensor.TYPE_GYROSCOPE:
	        // process gyro data
	        //gyroFunction(event); //blocked
	        gyroFunction_OSCPositionManager(event);
	        break;
	 
	    case Sensor.TYPE_MAGNETIC_FIELD:
	        // copy new magnetometer data into magnet array
	        System.arraycopy(event.values, 0, magnet, 0, 3);
	        break;
	    }
		task.run();
	}
	
	/**
	 * created on 2014-10-06, updated on 2014-10-09
	 * used for vuforia-recognition
	 * @param event
	 */
	private void Vuforia_recognition(SensorEvent event){
		//float x = event.values[0];//竖起来 -> 横起来
		float y = event.values[1];//9,8...0.1,-0.1
		float z = event.values[2];//0,1...8.9
		float y_ShuValue = 8.9f; //border 横竖的界限;
		
		Acceleration_Y = y;
		Acceleration_Y_filter = 0.1f*y + 0.9f*Acceleration_Y_filter;//过滤器哈
		
		if(Acceleration_Y_filter>=y_ShuValue)
			MSLConfig.Shu = true;
		else
			MSLConfig.Shu = false;
		
		Log.v("tag", "pointer4accy/" + y);//9.81
		Log.v("tag", "pointer4accz/" + z);
		
		//TODO 应该改变handler中send vibration message的策略？？？
		if(Acceleration_Y_filter>=y_ShuValue && MSLConfig.screen!=-1  &&  !MSLConfig.recognize){
			MSLConfig.recognize = true;
			Log.v("tag", "tngvuforiaa/****************** ready");
			
			//Toast.makeText(GantLondon0_pointer4pieces.con, MSLConfig.screenName + " is Ready !", Toast.LENGTH_SHORT).show();
			//GantLondon0_pointer4pieces.updateBackgroundImages_handler.obtainMessage(GantLondon0_pointer4pieces.MSG_MSL_OSC_Vibration).sendToTarget();
			
			//ifPointer4PiecesCalibrating = true;//TODO start to pointer4pieces-calibration;
		}else if(MSLConfig.screen==-1 && Acceleration_Y_filter>=y_ShuValue){
			Log.v("tag", "tngvuforiaa/*");
			MSLConfig.recognize = false;
		}
		
		/*Log.v("tag", "tngvuforiaa/*");
		if(MSLConfig.screen==-1 && y>=8 && !MSLConfig.recognizing){//start to recognize the vuforia
			Log.v("tag", "tngvuforiaa/******************");
			Toast.makeText(GantLondon0_pointer4pieces.con, "start", Toast.LENGTH_SHORT).show();
			MSLConfig.recognizing = true;
		}
		if(y<8){
			MSLConfig.recognizing = false;
		}*/
	}
	
	// calculates orientation angles from accelerometer and magnetometer output
	public void calculateAccMagOrientation() {
	    if(SensorManager.getRotationMatrix(rotationMatrix, null, accel, magnet)) {
	        SensorManager.getOrientation(rotationMatrix, accMagOrientation);
	    }
	}
	
	// This function is borrowed from the Android reference
	// at http://developer.android.com/reference/android/hardware/SensorEvent.html#values
	// It calculates a rotation vector from the gyroscope angular speed values.
    private void getRotationVectorFromGyro(float[] gyroValues,
            float[] deltaRotationVector,
            float timeFactor)
	{
		float[] normValues = new float[3];
		
		// Calculate the angular speed of the sample
		float omegaMagnitude =
		(float)Math.sqrt(gyroValues[0] * gyroValues[0] +
		gyroValues[1] * gyroValues[1] +
		gyroValues[2] * gyroValues[2]);
		
		// Normalize the rotation vector if it's big enough to get the axis
		if(omegaMagnitude > EPSILON) {
		normValues[0] = gyroValues[0] / omegaMagnitude;
		normValues[1] = gyroValues[1] / omegaMagnitude;
		normValues[2] = gyroValues[2] / omegaMagnitude;
		}
		
		// Integrate around this axis with the angular speed by the timestep
		// in order to get a delta rotation from this sample over the timestep
		// We will convert this axis-angle representation of the delta rotation
		// into a quaternion before turning it into the rotation matrix.
		float thetaOverTwo = omegaMagnitude * timeFactor;
		float sinThetaOverTwo = (float)Math.sin(thetaOverTwo);
		float cosThetaOverTwo = (float)Math.cos(thetaOverTwo);
		deltaRotationVector[0] = sinThetaOverTwo * normValues[0];
		deltaRotationVector[1] = sinThetaOverTwo * normValues[1];
		deltaRotationVector[2] = sinThetaOverTwo * normValues[2];
		deltaRotationVector[3] = cosThetaOverTwo;
	}
    
    /**
     * self-definition, used to send OSC-position message;
     * @param event
     */
    public static void setIfSendOSCPositionMessage(boolean ifSend) {
    	ifSendOSCPositionMessage = ifSend;
    }
    
    public boolean getIfKeepingXYPointerStill(){
    	return ifKeepingXYPointerStill;
    }
    
    /**
     * 自定义，用于是否保持 X Y 左边still静止
     * @param ifKeep
     */
    public static void setIfKeepingXYPointerStill (boolean ifKeep){
    	ifKeepingXYPointerStill = ifKeep;
    }
    
    public void gyroFunction_OSCPositionManager(SensorEvent event) {
    	Log.v("coordinate", "still/if send/" + ifSendOSCPositionMessage);
		
    	if(!ifSendOSCPositionMessage)
    		return;
    	
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
			float dT = 0;
			if (timestamp != 0) {
				dT = (event.timestamp - timestamp) * NS2S;
				angle_x = (int) (event.values[X_Pointer_index] * 100);// take two digits after decimal amount, multiply by 100, convert to Integer
				angle_y = (int) (event.values[Y_Pointer_index] * 100);
				angle[X_Pointer_index] = angle[X_Pointer_index] + angle_x * dT;
				angle[Y_Pointer_index] = angle[Y_Pointer_index] + angle_y * dT;
				angle_pointer[X_Pointer_index] = 0.3f * 23 * angle[X_Pointer_index] + 0.7f* angle_pointer[X_Pointer_index];// TODO 26 is the coefficient to covert to coordinate
				angle_pointer[Y_Pointer_index] = 0.3f * 18 * angle[Y_Pointer_index] + 0.7f * angle_pointer[Y_Pointer_index];// 26 is the coefficient to covert to coordinate
			}
			timestamp = event.timestamp;
			x_pointer = (angle_pointer[X_Pointer_index] * (-1))/100f;// divide it by 100
			y_pointer = (angle_pointer[Y_Pointer_index]) / 100f;
			
			msg = new OSCMessage("/phone1");
			
			if(ifKeepingXYPointerStill){ //静止状态，发送此状态之前的x y坐标值
				msg.addArgument(x_pointer_still + pointerShiftX);// divide it by 100
				msg.addArgument(y_pointer_still);
				Log.v("coordinate", "stillpointer YES" +x_pointer_still+"/"+y_pointer_still);
			}else {
				//记录最新的 x & y 的坐标值
				x_pointer_still = x_pointer;
				y_pointer_still = y_pointer;
				
				//parameters for pointer4pieces
				//TODO make these parameters global static
				float x_left = -10f;
				float x_right = -4.6f;//-4.6f //Unity-screen border & 1or2 screens
				float y_up = 10f;
				float y_down = -10f;
				float y_height = 20f;
				float y_half_height = 10f;
				
				/**
				 * here is the border-interpretation
				 * block if only want poster-screen work for pointer4pieces
				 * un-block if want both poster & mannequin work for pointer4pieces
				 */
				switch(MSLConfig.screen){
				case 1://gant-poster
					x_left = -10f;
					x_right = -4.6f;//-4.6f //Unity-screen border & 1or2 screens
					break;
				case 2://gant-mannequin, man-mannequin
					x_left = -2.6f;
					x_right = 2.6f;//-4.6f //Unity-screen border & 1or2 screens
					break;
				default:
					x_left = -10f;
					x_right = -4.6f;
					break;
				}
				
				//Pointer-4-Pieces
				//if(ifPointer4PiecesCalibrating && MSLConfig.screen==1){
				if(ifPointer4PiecesCalibrating ){//此时为: pointer4pieces now
					float xx1 = (x_left) + (9.8f-Acceleration_Y_filter)/9.8f * (x_right-x_left)/2f;//-10f为screen最左边的X坐标, -4.6为screen的最右边的X左边
					float yy1 = y_up - (9.8f-Acceleration_Y_filter)/9.8f * y_half_height; //第一个10f为screen最高点的Y坐标；第二个10f为screen的总高的一半
					float xx2 = x_left + x_right - xx1;
					float yy2 = yy1;
					float xx3 = xx1;
					float yy3 = -yy1;
					float xx4 = xx2;
					float yy4 = -yy1;
					
					if(yy1>yy4){ //still calibration model for pointer-4-pieces
						msg.addArgument(xx1);//左上角
						msg.addArgument(yy1);
						msg.addArgument(xx2);//右上角
						msg.addArgument(yy2);
						msg.addArgument(xx3);//左下角
						msg.addArgument(yy3);
						msg.addArgument(xx4);//右下角
						msg.addArgument(yy4);
					}
					else if(yy1<=yy4){ //4 pieces' pointer gathering in the center now...
						ifPointer4PiecesCalibrating=false;
						//TODO re-calibrate x,y coordinate, set to the center
						SensorFusionActivity0.recalibrate();
						SensorFusionActivity0.chooseScreen_Vuforia2Screens(MSLConfig.screen);
						msg.addArgument(0f + pointerShiftX );//to the center
						msg.addArgument(0f);//to the center, ???
					}
				}else if(!ifPointer4PiecesCalibrating){ //此时为: normal status
					
					/**
					 * on 2014-11-25 prepare for Shelf in Helsinki
					 * 像鼠标一样设置border
					 */
					float x1 = -4.9f;
					float x2 = -1.1f;
					float y1 = 9.9f;
					float y2 = -9.9f;
					float x = x_pointer + pointerShiftX - pointerMouseReferX;
					float y = y_pointer + pointerShiftY - pointerMouseReferY;
					if(x<x1){
						pointerMouseReferX = x_pointer + pointerShiftX - (x1);
					}else if(x>x2){
						pointerMouseReferX = x_pointer + pointerShiftX - x2;
					}
					if(y>y1){
						pointerMouseReferY = y_pointer + pointerShiftY - y1;
					}else if(y<y2){
						pointerMouseReferY = y_pointer + pointerShiftY - (y2);
					}
					
					//TODO 2014-12-04 Mouse-stuff
					//msg.addArgument(x_pointer + pointerShiftX - pointerMouseReferX);
					//msg.addArgument(y_pointer + pointerShiftY - pointerMouseReferY);
					
					msg.addArgument(x_pointer + pointerShiftX );
					msg.addArgument(y_pointer + pointerShiftY );
					
					x_pointer = x_pointer + pointerShiftX; //added on 2014-09-12,因为要更新imageBackground
				}else{ //以防万一;in case
					msg.addArgument(x_pointer + pointerShiftX );
					msg.addArgument(y_pointer);
					x_pointer = x_pointer + pointerShiftX; //added on 2014-09-12,因为要更新imageBackground
				}
				
				Log.v("pointer4", "pointer4x:/" + x_pointer);
				Log.v("pointer4", "pointer4y:/" + y_pointer);
			}
			
			x_pointer_pre = x_pointer;
			
			try { 
				this.sender_TV.send(msg);//TODO 2014-11-06 shelf
				this.sender.send(msg);//mannequin
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
    }
    
    public static void chooseScreen(int screen){
    	//Log.v("dir", "dir/1/~~"+MSLConfig.direction_value);
    	if(screen==1){
    		pointerShiftX = -7.333f;
    	}else if(screen==3){
    		pointerShiftX = 7.333f;
    	}else pointerShiftX = 0;
    }
    
    //added on 2014-10-08, for vuforia recognition for items/images
    public static void chooseScreen_Vuforia2Screens(int screen){
    	if(screen==1){
    		pointerShiftX = -7.333f;
    	}else if(screen==2){
    		pointerShiftX = 0f;
    	}
    	//pointerShiftX += 7.333f; //2014-10-14 Flipis Unity-screen border & 1or2 screens
    }
    
    //added on 2014-09-10, for 2014-09-15 Monday night
    public static void chooseScreenOnlyForMondayDemo(int screen){
    	if(screen==1){
    		pointerShiftX = -5f;//-7.333f
    		Log.v("dir", "dir/1/"+MSLConfig.direction_value);
    	}else if(screen==3){
    		pointerShiftX = 7.333f;
    	}else {
    		pointerShiftX = 5f;
    		Log.v("dir", "dir/2/"+MSLConfig.direction_value);
    	}
    }
	
    private float[] getRotationMatrixFromOrientation(float[] o) {
        float[] xM = new float[9];
        float[] yM = new float[9];
        float[] zM = new float[9];
     
        float sinX = (float)Math.sin(o[1]);
        float cosX = (float)Math.cos(o[1]);
        float sinY = (float)Math.sin(o[2]);
        float cosY = (float)Math.cos(o[2]);
        float sinZ = (float)Math.sin(o[0]);
        float cosZ = (float)Math.cos(o[0]);
     
        // rotation about x-axis (pitch)
        xM[0] = 1.0f; xM[1] = 0.0f; xM[2] = 0.0f;
        xM[3] = 0.0f; xM[4] = cosX; xM[5] = sinX;
        xM[6] = 0.0f; xM[7] = -sinX; xM[8] = cosX;
     
        // rotation about y-axis (roll)
        yM[0] = cosY; yM[1] = 0.0f; yM[2] = sinY;
        yM[3] = 0.0f; yM[4] = 1.0f; yM[5] = 0.0f;
        yM[6] = -sinY; yM[7] = 0.0f; yM[8] = cosY;
     
        // rotation about z-axis (azimuth)
        zM[0] = cosZ; zM[1] = sinZ; zM[2] = 0.0f;
        zM[3] = -sinZ; zM[4] = cosZ; zM[5] = 0.0f;
        zM[6] = 0.0f; zM[7] = 0.0f; zM[8] = 1.0f;
     
        // rotation order is y, x, z (roll, pitch, azimuth)
        float[] resultMatrix = matrixMultiplication(xM, yM);
        resultMatrix = matrixMultiplication(zM, resultMatrix);
        return resultMatrix;
    }
    
    private float[] matrixMultiplication(float[] A, float[] B) {
        float[] result = new float[9];
     
        result[0] = A[0] * B[0] + A[1] * B[3] + A[2] * B[6];
        result[1] = A[0] * B[1] + A[1] * B[4] + A[2] * B[7];
        result[2] = A[0] * B[2] + A[1] * B[5] + A[2] * B[8];
     
        result[3] = A[3] * B[0] + A[4] * B[3] + A[5] * B[6];
        result[4] = A[3] * B[1] + A[4] * B[4] + A[5] * B[7];
        result[5] = A[3] * B[2] + A[4] * B[5] + A[5] * B[8];
     
        result[6] = A[6] * B[0] + A[7] * B[3] + A[8] * B[6];
        result[7] = A[6] * B[1] + A[7] * B[4] + A[8] * B[7];
        result[8] = A[6] * B[2] + A[7] * B[5] + A[8] * B[8];
     
        return result;
    }
    
    class calculateFusedOrientationTask {
    //extends TimerTask {
        public void run() {
            float oneMinusCoeff = 1.0f - FILTER_COEFFICIENT;
            
            /*
             * Fix for 179� <--> -179� transition problem:
             * Check whether one of the two orientation angles (gyro or accMag) is negative while the other one is positive.
             * If so, add 360� (2 * math.PI) to the negative value, perform the sensor fusion, and remove the 360� from the result
             * if it is greater than 180�. This stabilizes the output in positive-to-negative-transition cases.
             */
            
            // azimuth
            if (gyroOrientation[0] < -0.5 * Math.PI && accMagOrientation[0] > 0.0) {
            	fusedOrientation[0] = (float) (FILTER_COEFFICIENT * (gyroOrientation[0] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[0]);
        		fusedOrientation[0] -= (fusedOrientation[0] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[0] < -0.5 * Math.PI && gyroOrientation[0] > 0.0) {
            	fusedOrientation[0] = (float) (FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * (accMagOrientation[0] + 2.0 * Math.PI));
            	fusedOrientation[0] -= (fusedOrientation[0] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
            	fusedOrientation[0] = FILTER_COEFFICIENT * gyroOrientation[0] + oneMinusCoeff * accMagOrientation[0];
            }
            
            // pitch
            if (gyroOrientation[1] < -0.5 * Math.PI && accMagOrientation[1] > 0.0) {
            	fusedOrientation[1] = (float) (FILTER_COEFFICIENT * (gyroOrientation[1] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[1]);
        		fusedOrientation[1] -= (fusedOrientation[1] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[1] < -0.5 * Math.PI && gyroOrientation[1] > 0.0) {
            	fusedOrientation[1] = (float) (FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * (accMagOrientation[1] + 2.0 * Math.PI));
            	fusedOrientation[1] -= (fusedOrientation[1] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
            	fusedOrientation[1] = FILTER_COEFFICIENT * gyroOrientation[1] + oneMinusCoeff * accMagOrientation[1];
            }
            
            // roll
            if (gyroOrientation[2] < -0.5 * Math.PI && accMagOrientation[2] > 0.0) {
            	fusedOrientation[2] = (float) (FILTER_COEFFICIENT * (gyroOrientation[2] + 2.0 * Math.PI) + oneMinusCoeff * accMagOrientation[2]);
        		fusedOrientation[2] -= (fusedOrientation[2] > Math.PI) ? 2.0 * Math.PI : 0;
            }
            else if (accMagOrientation[2] < -0.5 * Math.PI && gyroOrientation[2] > 0.0) {
            	fusedOrientation[2] = (float) (FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * (accMagOrientation[2] + 2.0 * Math.PI));
            	fusedOrientation[2] -= (fusedOrientation[2] > Math.PI)? 2.0 * Math.PI : 0;
            }
            else {
            	fusedOrientation[2] = FILTER_COEFFICIENT * gyroOrientation[2] + oneMinusCoeff * accMagOrientation[2];
            }
     
            // overwrite gyro matrix and orientation with fused orientation
            // to comensate gyro drift
            gyroMatrix = getRotationMatrixFromOrientation(fusedOrientation);
            System.arraycopy(fusedOrientation, 0, gyroOrientation, 0, 3);
            
            //---------------------------------------------
            if(2>0)
            	return;
            //---------------------------------------------
            
            // update sensor output in GUI
            //mHandler.post(updateOreintationDisplayTask);
        }
    }
    
	public static void recalibrate() { //starting point
		angle[X_Pointer_index] = 0f;
		angle[Y_Pointer_index] = 0f;
		angle_pointer[X_Pointer_index] = 0f;
		angle_pointer[Y_Pointer_index] = 0f;
		pointerMouseReferX = 0.0f;
		pointerMouseReferY = 0.0f;
		Log.v("sensorcalibrate", "recalibrate");
	}
    
    
    // **************************** GUI FUNCTIONS *********************************

    
    public void updateOreintationDisplay() {
    	Log.v("test",d.format(fusedOrientation[0] * 180/Math.PI) + "****" );
    	
    	/*switch(radioSelection) {
    	case 0:
    		mAzimuthView.setText(d.format(accMagOrientation[0] * 180/Math.PI) + '�');
            mPitchView.setText(d.format(accMagOrientation[1] * 180/Math.PI) + '�');
            mRollView.setText(d.format(accMagOrientation[2] * 180/Math.PI) + '�');
    		break;
    	case 1:
    		mAzimuthView.setText(d.format(gyroOrientation[0] * 180/Math.PI) + '�');
            mPitchView.setText(d.format(gyroOrientation[1] * 180/Math.PI) + '�');
            mRollView.setText(d.format(gyroOrientation[2] * 180/Math.PI) + '�');
    		break;
    	case 2:
    		mAzimuthView.setText(d.format(fusedOrientation[0] * 180/Math.PI) + '�');
            mPitchView.setText(d.format(fusedOrientation[1] * 180/Math.PI) + '�');
            mRollView.setText(d.format(fusedOrientation[2] * 180/Math.PI) + '�');
    		break;
    	}*/
    }
    
    private Runnable updateOreintationDisplayTask = new Runnable() {
		@Override
		public void run() {
			updateOreintationDisplay();
		}
	};
}
package com.livingnetworks.pl;

import java.util.ArrayList;

import android.app.Activity;
import android.view.View;
import android.view.View.OnSystemUiVisibilityChangeListener;

public class MSLConfig {
	
	/**
	 * 此boolean意味着手指是否放手，true为放手 即手指已离开button，false为 未放手 即手放在button上
	 */
	public static boolean ifFangShou = true;
	
	/**
	 * for recognition vuforia-images/items;
	 */
	public static boolean recognize = false;//if true; if false
	public static boolean Shu = true;//if true, means手机现在竖立; if falses, means手机现在 没有竖立
	public static String screenName = "null";//such as "poster","mannequin"
	
	public static String OscMsgFromOSCServer = "1";//å½“clienté€‰æ‹©select objectæ—¶ï¼Œç›¸åº”çš„ä»ŽOSC-serverè¿”å›žçš„id
	public static boolean updateMainUI = false;//onTouch up/down
	
	public static final String TempURL_shelf_arduino = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=random&location=2342,2342";
	public static final String TempURL_philips = "http://192.168.0.80/database.json";
	public static final String TempURL_philips_2 = "http://192.168.2.2:8080/database.json";
	public static final String TempURL = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=north&location=2342,2342";//tell K if you want change anything
	public static final String TempURL_BigShelf = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=anydirection&location=1423,3241";//"BigShelf"
	
	public static final String TempURL_Shelf_Helsinki = "http://192.168.2.130/database1.json";
	
	public static final String livingnetworksURL = "http://www.kth.se/en/ict/forskning/cos/research/mslab/mobile-service-laboratory-1.343995";
	public static final String TempBea = "nobeacon";
	
	public static int screen = 1;
	public static int screenOnlyForMondayDemo = 1;
	public static String SCREEN1 = "screen1";//stones
	//public static String SCREEN2 = "screen_man";//screen_Z screen_man chips screen2
	public static String SCREEN2 = "screen_man";
	
	//public static Location LOCATION = new Location(LocationManager.NETWORK_PROVIDER);
	public final static double LATITUDE = 59.405190d;
	public final static double LONGITUDE = 17.949192d;
	public static double latitude = 0.0d;
	public static double longitude= 0.0d;
	public static float[] distance = new float[]{0.0f,0.0f,0.0f,};

	public static double direction_value = 0;
	public static double direction_value_rec = 0;//过滤后的value;
	//public static double direction_value_rec_pre = 0;//记录之前的value;
	public static String direction = "none";
	public static String get_direction_url = "http://msl1.it.kth.se/contentmanager/getcontent.json?direction=";
	public static String get_location_mark = "&location=56.2408019,-76.680299";
	public static String player_url = "http://130.237.238.176:81/Command.aspx?format=json&upid=0001807aadc8&command=getPlayingFiles";
	
	public static String server_ip = "130.229.168.216";
	public static int server_port = 50001;
	public static int phoneport = 8000;
	public static String server_ip_tv = "192.168.1.2";//for TNG-tv
	public static final int PHONEPORT_8000 = 8000; //固定的8000
	
	public static float x;
	public static float y;
	public static String direction_previous = "none";
	
	public static double direction_value2 = 0;//dengyu direction_value, zhishi butong de lei.
	public static double direction_convertered2 = 0;
	public static double converter = 0;
	public static ArrayList <Integer> drawableList = new ArrayList <Integer> ();
    public static boolean finishInitialize_ImageViews = false;
    
    public static String Timestamp;
    public static String StartTime;
    
    public static void removeNavBar(final Activity activity){
    	String str = "/"+android.os.Build.MANUFACTURER +"/"+android.os.Build.BRAND+"/"+android.os.Build.MODEL+"/";
    	if(!str.contains("Nexus 5"))
    		return;
    	
		View decorView = activity.getWindow().getDecorView();
		int uiOptions =  View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
               | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
               | View.SYSTEM_UI_FLAG_FULLSCREEN
               | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
		decorView.setSystemUiVisibility(uiOptions);
		
		decorView.setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener() {
			@Override
			public void onSystemUiVisibilityChange(int visibility) {
				int uiOptions =  View.SYSTEM_UI_FLAG_LAYOUT_STABLE
		                 | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
		                 | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
		                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
		                | View.SYSTEM_UI_FLAG_FULLSCREEN
		                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
				activity.getWindow().getDecorView().setSystemUiVisibility(uiOptions);
			}
		});
    } //end removeNavBar
}
package com.livingnetworks.pl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortIn;
import com.illposed.osc.OSCPortOut;
import com.livingnetworks.demo2.R;

@SuppressLint("NewApi")
public class GantLondon0 extends Activity implements View.OnClickListener{
	
	private static Vibrator vibe;

	final String TAG = "GantLondon";
	final String BUGS= "bugs";
	
	private OSCPortIn server;// phone server
	private static OSCPortOut mSender;
	private static OSCPortOut mSender_TV;
	static String oscConnectText = "/phone1/bluebutton";
	String oscSelectText = "/phone1/bigbutton";
	
	private static JSONObject objectlist;//(contentid,interactiontype)
	private JSONArray namelist;
	private static JSONObject contentBOX;//记录 content的各种操蛋信息
	static OSCMessage msg = null;

	private static SensorManager mSensorManager;
	static SensorFusionActivity0 sf;
	public Context con;
	Activity acc;
	
	private static FragmentManager fragmentManager;
	
	//private ImageButton help;
	private ImageButton pointer;
	private static ImageButton visualsearch;
	
	private static boolean edit_mode = false;
	
	/**
	 * activity result code
	 */
	static final int ACTIVITYRESULT_REQUESTCODE_WEBVIEW = 1;
	static final int ACTIVITYRESULT_REQUESTCODE_ABOUT = 2;
	static final int ACTIVITYRESULT_REQUESTCODE_VISUALSEARCH = 3;
	
	/**
	 * slider part
	 */
    public static final int MSG_LOCK_SUCESS = 2;//not really in use;
    public static final int MSG_LOCK_OSC_Connect = 3;//message.what connect-command;
    public static final int MSG_LOCK_OSC_Select = 4;//message.what select-command;
    public static final int MSG_LOCK_OSC_Disconnect = 5;//message.what disconnect-command;
    
    private Handler lock_handler =new Handler (new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			switch (msg.what){
			case MSG_LOCK_SUCESS:
				Log.v(TAG, "unlock message");
				break;
			case MSG_LOCK_OSC_Connect:
				if(!isWangluoKeyong()){
					return false;
				}
				DisPointerStill();
				PointConnectFunction();
				Log.v(BUGS, "PointConnectFunction");
				break;
			case MSG_LOCK_OSC_Select:
				Log.v("test", "msg select");
				if(!isWangluoKeyong()){
					return false;
				}
				
				SelectFunction();
				//当fingerrelease后，即 pointer-btn有onTouch-up时，先Disconnect，即不再发送x&y坐标
				//然后若收到feedback-command from OSC-server，则Connect&PointerStill...
				Disconnect();//RELEASE TODO if release finger, which is onTouch-up, disconnect first;
				Log.v(BUGS, "SelectFunction & PointerStill");
				break;
			case MSG_LOCK_OSC_Disconnect:
				if(!isWangluoKeyong()){
					return false;
				}
				Disconnect();
				Log.v(BUGS, "Disconnect & DisconnectOSCCommand");
				break;
			default:
				break;
			}
			return false;
		}
	});
    
    public Drawable getImage(Context context, String name) {
        return context.getResources().getDrawable(context.getResources().getIdentifier(name, "drawable", context.getPackageName()));
        }
	
    public static final int MSG_MSL_OSC_ResponseFromOSCServer = 1;//当发送select时，收到从OSC-server发送的command
	@SuppressLint("HandlerLeak")
	public Handler msl_handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_MSL_OSC_ResponseFromOSCServer://当点击select时,在looplistener中收到osc-server中发送的id的处理...
				//TODO edit
				String str = "";
				JSONObject json = new JSONObject();//此时json即为应该edit的东西哈
				try {
					json = contentBOX.getJSONObject(MSLConfig.OscMsgFromOSCServer);
					Log.v("content", "contentbox/"+json.getString("name")+"/"+json.getString("id")+"/"+objectlist.getString("id"));
					}
				catch (JSONException e1) {str = "error000"; e1.printStackTrace();}
				Log.v("content", "contentbox/"+str+"/"+json.toString());
				
				if(edit_mode){
					edit("/edit");//TODO
					CreatedTileShowInfoDialog dia = new CreatedTileShowInfoDialog("d",json,(String)msg.obj);
					dia.show(fragmentManager, "");
				}else{
					Intent intent = new Intent(con, GantWebView.class);
					intent.putExtra("weburl", "" + (String)msg.obj);
					acc.startActivityForResult(intent, ACTIVITYRESULT_REQUESTCODE_WEBVIEW);
				}
				
				//如果select后,接收到相应的OSC-command from OSC-server，则需要重新connect，但是要PointerStill
				Connect();
				PointerStill();//RELEASE TODO if release finger, which is onTouch-up, disconnect first;
				
				break;
			case 11:
				DisconnectOSCCommand();//发送disconnect OSC-command
				break;
			default: break;
			}
			super.handleMessage(msg);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gantlondon);
		
		acc = this;
		con = this;
		fragmentManager  = this.getFragmentManager();
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		
		pointer = (ImageButton)findViewById(R.id.pointer);
		pointer.setBackgroundResource(R.drawable.point_button);
		pointer.setOnTouchListener(new onPointerButtonTouch());
		visualsearch = (ImageButton)findViewById(R.id.visualsearch);
		visualsearch.setBackgroundResource(R.drawable.visualsearch_button);
		visualsearch.setOnClickListener(this);
		visualsearch.setVisibility(View.INVISIBLE);//TODO
		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
	} // onCreate
	
	public void PointConnectFunction() {
		try {
			SensorFusionActivity0.recalibrate();
			String str_server = "";
			new FetchPlayListFromContentmanager(str_server).execute(MSLConfig.TempURL_BigShelf);//TempURL_Shelf_Helsinki
		} catch (Exception e) {
			Log.v("lalala", "exception happens 11111111111");
			e.printStackTrace();
		}
		MSLConfig.direction_previous = MSLConfig.direction;
	}
	//****************************************
	//TODO for AsyncTask
	private void initializeConfigValue(String str_server){
		try {
			Log.v("lalala", "1/"+MSLConfig.get_direction_url+ MSLConfig.direction + MSLConfig.get_location_mark);
			JSONObject jobj = new JSONObject(str_server);
			
			Log.v("lalala", "serverip: "+jobj.getJSONArray("poster").getJSONObject(0).getString("serverip").toString());
			Log.v("lalala", "serverport: "+jobj.getJSONArray("poster").getJSONObject(0).getInt("serverport"));
			Log.v("lalala", "phoneport: "+jobj.getJSONArray("poster").getJSONObject(0).getInt("phoneport"));
			Log.v("lalala", "+++ "+jobj.getJSONArray("poster").getJSONObject(0).toString());
			Log.v("lalala", "--- "+jobj.getJSONArray("content").getJSONArray(0).toString());
			Log.v("lalala", "postertype: "+jobj.getJSONArray("poster").getJSONObject(0).getString("postertype"));
			
			createContentList(jobj.getJSONArray("content").getJSONArray(0),  jobj.getJSONArray("poster").getJSONObject(0));
			
			MSLConfig.server_ip = jobj.getJSONArray("poster").getJSONObject(0).getString("serverip").toString();
			MSLConfig.server_port = jobj.getJSONArray("poster").getJSONObject(0).getInt("serverport");
			MSLConfig.phoneport = jobj.getJSONArray("poster").getJSONObject(0).getInt("phoneport");
			MSLConfig.server_ip_tv = jobj.getJSONArray("poster").getJSONObject(0).getString("beacon").toString();
			
			Log.v("lalala", "/*/"+MSLConfig.server_ip + " " + MSLConfig.server_port + " " + MSLConfig.phoneport);
			
			new connectToServerAsyncTask().execute("");
		} catch (Exception e) {
			Log.v("lalala", "exception happens 22222222222");
			e.printStackTrace();
		}
	}
	//****************************************
	//****************************************
	public class FetchPlayListFromContentmanager extends AsyncTask<String, String, String> {
	    //String playlist;
	    JSONObject poster;
		
	    public FetchPlayListFromContentmanager(String list) {
	    	//this.playlist = list;
	    }
	    
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	    }
	    
	    @Override
		protected String doInBackground(String... urls) {
	    	String str_server = "";
	    	try {
	    		str_server = sendConnectGet(urls[0]);
	    		Log.v("lalala", "url/" + MSLConfig.get_direction_url+ MSLConfig.direction + MSLConfig.get_location_mark + MSLConfig.latitude+","+MSLConfig.longitude);
				Log.v("lalala", "GLF_playlist*/"+ str_server);
			} catch (Exception e1) {
				Log.v("lalala", "exception happens 3333333333");
				e1.printStackTrace();
			}
	    	return str_server;
		}
	    
	    @Override
		protected void onPostExecute(String str_server) {
	    	initializeConfigValue(str_server);//不能将此method放到doInBackground中进行!!...
	    }
	    
	    private String sendConnectGet(String url0) throws Exception {
			try{
				URL obj = new URL(url0);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("GET");
				int responseCode = con.getResponseCode();
				System.out.println("\nSending 'GET' request to URL : " + url0);
				System.out.println("Response Code : " + responseCode);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				return response.toString();
			}catch(Exception ex){
				Log.v("lalala", "exception happens 44444444");
				return "";
			}
	    }
		}
	//****************************************
	//****************************************
	public class connectToServerAsyncTask extends AsyncTask<String,String,String>{
		
		@Override
		protected void onPreExecute() {
			SensorFusionActivity0.recalibrate();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			/**
			 * 2014-11-06
			 * 因为他妈的突然快速 按button然后 突然离开，pointer仍然停留在poster上
			 */
			if(MSLConfig.ifFangShou){
				Disconnect();
				return "";
			}
			
			try {
				connectToServer(MSLConfig.server_ip, MSLConfig.server_port,MSLConfig.PHONEPORT_8000);
			} catch (InterruptedException e) {
				Log.v("lalala", "exception happens connectToServerAsyncTask");
				e.printStackTrace();
			}
			Log.v("lalala", "/*111/");
			startOSCListening();
			Log.v("lalala", "/*222/");
			startListeners();
			
			/**
			 * 2014-11-06
			 * 因为他妈的突然快速 按button然后 突然离开，pointer仍然停留在poster上
			 */
			if(MSLConfig.ifFangShou){
				Disconnect();
			}else {
				Connect();
			}
			
			Log.v("lalala", "/*333/");
			return "";
		}
		
		@Override
		protected void onPostExecute(String result) {
			//startListeners();
			super.onPostExecute(result);
		}
		
	}
	//****************************************
	//****************************************
	
	private void Disconnect(){
		SensorFusionActivity0.setIfSendOSCPositionMessage(false);
		Log.v("test", "osc disconnect/"+SensorFusionActivity0.ifSendOSCPositionMessage);
	}
	private void Connect(){
		Log.v("test", "osc connect cmd");
		SensorFusionActivity0.setIfSendOSCPositionMessage(true);
	}
	
	//TODO used to send "disconnect" OSC-command...
	private void DisconnectOSCCommand(){
		try {
			OSCMessage msg = new OSCMessage("/disconnect");
			msg.addArgument("/phone1");
			mSender.send(msg);
			mSender_TV.send(msg);
		} catch (Exception e) {
			Log.v("lalala", "exception happens 55555555555");
			e.printStackTrace();
		}
	}
	
	private void PointerStill(){
		SensorFusionActivity0.setIfKeepingXYPointerStill(true);
	}
	private void DisPointerStill(){
		SensorFusionActivity0.setIfKeepingXYPointerStill(false);
	}
	
	public void SelectFunction() {
		if (mSender == null || mSender_TV == null)// 检测刚开始时，mSender是否为null
		{
			Log.v("test", "osc selectFunction / mSender null");
			return;
		}
		OSCMessage msg = new OSCMessage(oscSelectText);
		msg.addArgument("/phone1");
		try {
			mSender.send(msg);
			mSender_TV.send(msg);
			Log.v("test", "osc selectFunction clicked");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.visualsearch){
			Intent intent = new Intent(GantLondon0.this, ShelfVisualSearch.class);
			startActivityForResult(intent, ACTIVITYRESULT_REQUESTCODE_VISUALSEARCH);
		}
	}
	
	public class onPointerButtonTouch implements View.OnTouchListener {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			
			if(event.getAction() == MotionEvent.ACTION_DOWN) {
				MSLConfig.ifFangShou = false;
				lock_handler.obtainMessage(GantLondon0.MSG_LOCK_OSC_Connect).sendToTarget();
			}else if(event.getAction() == MotionEvent.ACTION_UP){
				MSLConfig.ifFangShou = true;
				lock_handler.obtainMessage(GantLondon0.MSG_LOCK_OSC_Select).sendToTarget();
			}
			
			return false;
		}
	}
	
	public class ExitFragment extends DialogFragment{
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			this.setCancelable(false);
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("Exit this app ? ")
					.setPositiveButton("Exit",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int id) {
									acc.finish();
								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int id) {
								}
							});
			return builder.create();
		}
	}
	
	private void createContentList(JSONArray array, JSONObject poster) throws JSONException {
		objectlist = new JSONObject();
		namelist = new JSONArray();
		contentBOX = new JSONObject();//TNG
		objectlist.put("postertype", poster.getString("postertype"));
		objectlist.put("id", poster.getString("id"));
		for (int i = 0; i < array.length(); i++) {
			objectlist.put(array.getJSONObject(i).getString("contentid"), array.getJSONObject(i).getString("interactiontype"));
			contentBOX.put(array.getJSONObject(i).getString("contentid"), array.getJSONObject(i));//TNG
			try{
				Long.parseLong(((JSONObject)array.get(i)).getString("name"));
				namelist.put(i,array.get(i) );
			}catch(NumberFormatException ex){
				ex.printStackTrace();
				continue;
			}
		}
		Log.v("test", "there are total of  objects"+ namelist.length());
	}
	
	//***
	protected void connectToServer(String server_ip, int server_port, int phoneport) throws InterruptedException {
		SensorFusionActivity0.recalibrate();
		Log.v("vuforia", "screen no."+ MSLConfig.screen);
		//SensorFusionActivity0.chooseScreenOnlyForMondayDemo(MSLConfig.screenOnlyForMondayDemo);//shift; for Monday's Demo, 09-15的demo
		
		//SensorFusionActivity0_pointer4pieces_2IPs.chooseScreen_Vuforia2Screens(MSLConfig.screen);//shift;为了在使用Vuforia时,检测/选择正确的screen
		//SensorFusionActivity0_pointer4pieces_2IPs.chooseScreen(MSLConfig.screen);//shift;为了在使用Vuforia时,检测/选择正确的screen
		try {
			mSender = new OSCPortOut(InetAddress.getByName(MSLConfig.server_ip), MSLConfig.server_port);
			mSender_TV = new OSCPortOut(InetAddress.getByName(MSLConfig.server_ip_tv), MSLConfig.server_port);
			msg = new OSCMessage(oscConnectText);
			msg.addArgument("/phone1");
			mSender.send(msg);
			mSender_TV.send(msg);
		} catch (Exception e) {
			Log.v("test", "exception happens connectToServer");
			e.printStackTrace();
		}
	}

	private void startOSCListening() {
		stopOSCListening();
		try {
			server = new OSCPortIn(MSLConfig.PHONEPORT_8000);
			server.startListening();
		} catch (Exception e) {
			Log.v("lalala", "exception happens startOSCListening");
			return;
		}
		server.addListener("/playvideo", new LooperListener(GantLondon0.objectlist,msl_handler));
	}

	private void stopOSCListening() {
		if (server != null) {
			server.stopListening();
			server = null;
		}
	}
	
	private void startListeners() {
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if(sf!=null){
			sf.Stop();
		}
		sf = new SensorFusionActivity0(mSensorManager, mSender, mSender_TV);
	}
	//***

	@Override
	public void onLowMemory() {
		System.gc();
		Runtime.getRuntime().gc();
	};

	@SuppressLint("SetJavaScriptEnabled")
	public boolean performAction(String contentid, JSONObject objectlist) throws JSONException {
		if (objectlist.getString("postertype").equals("demotest")) { //projectorposter
			Message msg = new Message();
			msg.what = 1;
			msg.obj = objectlist.getString(contentid);
			msl_handler.sendMessage(msg);
			Log.v("GantLondon", "contentid: " + contentid);
		}
		return true;
	}

	@Override
	protected void onStart() {
		Log.v(TAG, "GantLondon onStart");
		super.onStart();
	}

	@Override
	protected void onResume() {
		MSLConfig.removeNavBar(this);
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		MSLConfig.removeNavBar(this);
		startOSCListening();
		super.onPause();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.v("test", "1//onActivityResult");
		Log.v(TAG, "onActivityResult: " + requestCode + " " + resultCode);
		if(requestCode==ACTIVITYRESULT_REQUESTCODE_WEBVIEW){
			//reinitializePointAndConnectView();
			lock_handler.obtainMessage(GantLondon0.MSG_LOCK_OSC_Disconnect).sendToTarget();
		}else if(requestCode==ACTIVITYRESULT_REQUESTCODE_ABOUT){
			//reinitializePointAndConnectView();
		}else if(requestCode == ACTIVITYRESULT_REQUESTCODE_VISUALSEARCH){
			//TODO
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		Log.v(TAG, "GantLondon onDestroy");
		if(sf!=null){
			sf.Stop();
			sf = null;
		}
		stopOSCListening();
		if(mSender != null)
			mSender = null;
		if(mSender_TV != null)
			mSender_TV = null;
		super.onDestroy();
	}
	
	public class CreatedTileShowInfoDialog extends DialogFragment {
		private String str;
		EditText et_tilename;
		int image = R.drawable.london_logo;
		
		private JSONObject json = new org.json.JSONObject();
		private String weburl = "";
		
		public CreatedTileShowInfoDialog(String id, JSONObject jso, String url) {
			this.str = id;
			this.json = jso;
			this.weburl = url;
		}
		public CreatedTileShowInfoDialog(String id, int imageID) {
			this.str = id;
			this.image = imageID;
		}
		
		@Override
		public void show(FragmentManager manager, String tag) {
			super.show(manager, tag);
		}
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			this.setCancelable(false);
			LayoutInflater inflater = getActivity().getLayoutInflater();
			ViewGroup vg = (ViewGroup)inflater.inflate(R.layout.createtile_popupdialog_edit, null);
			ImageView iv = (ImageView)vg.findViewById(R.id.createtile_imageview);
			iv.setBackgroundResource(this.image);
			et_tilename = (EditText)vg.findViewById(R.id.createtile_tilename_et);
			et_tilename.setText(weburl);//显示之前的url
			
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			//builder.setTitle("Show Tile Info.");
			builder.setMessage("Edit")
				   .setView(vg)
				   .setPositiveButton("Confirm",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								String str = et_tilename.getText().toString();
								if(str!=null){
									httpPut(json, str);
								}
							}
						})
						;
			return builder.create();
		}
	}//end CreatedTileShowInfoDialog
	
	public void edit(String address) {
		OSCMessage msg = new OSCMessage(address);
		msg.addArgument("address");
		try {
			mSender.send(msg);
			mSender_TV.send(msg);
			Log.v("test", "oscedit " + address);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void httpPut(JSONObject json, String path){
		try {
			String posterID = objectlist.getString("id");
			String contentID = json.getString("id");
			
			URI url = new URI("http://msl1.it.kth.se/contentmanager/posters/"+posterID+"/contentlists/"+contentID);
			HttpClient client = new DefaultHttpClient();
			HttpPut put= new HttpPut(url);
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("contentlist[interactiontype]", path));
			put.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse response = client.execute(put);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.layout.menu, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.action_edit:
	        	edit_mode = !edit_mode;
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private boolean isWangluoKeyong() {
	    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
}

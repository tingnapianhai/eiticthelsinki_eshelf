package com.livingnetworks.pl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.util.Log;

public class NetworkCommunication {
	
	public static String sendConnectGet(String url0) throws Exception {
		try{
			URL obj = new URL(url0);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url0);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}catch(Exception ex){
			return "";
		}
		
	}
	
	public static long getOffisideTVTime() throws Exception {
		URL obj = new URL(MSLConfig.player_url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		JSONObject time_jobj = new JSONObject(response.toString());
		MSLConfig.Timestamp = time_jobj.getString("Timestamp").toString();
		//MSLConfig.StartTime = time_jobj.getJSONObject("Result").getJSONObject("1").getJSONObject("1").getString("StartTime").toString();
		Long time  = time_jobj.getJSONObject("Result").getJSONObject("1").getJSONObject((String) time_jobj.getJSONObject("Result").getJSONObject("1").keys().next()).getLong("Offset");
		
		//long starttime = Long.parseLong(MSLConfig.StartTime.substring(0, 2))*3600*1000 + Long.parseLong(MSLConfig.StartTime.substring(3, 5))*60*1000 + Long.parseLong(MSLConfig.StartTime.substring(6, 8))*1000;
		//long timestamp = Long.parseLong(MSLConfig.Timestamp.substring(11, 13))*3600*1000 + Long.parseLong(MSLConfig.Timestamp.substring(14, 16))*60*1000 + Long.parseLong(MSLConfig.Timestamp.substring(17, 19))*1000;
		//Log.v("time", "time diff: " + (timestamp - starttime) + " offside: " + (timestamp - starttime)%MSLConfig.looptime);
		//return (timestamp - starttime)%MSLConfig.looptime;
		Log.v("test", time+"**********8");
		//return (time*1000)%MSLConfig.looptime;
		return time*1000;
		
	}
	
	public static long getOffisideTVTime(String url) throws Exception  {
		return getOffisideTVTime();
	}
	
}

package com.livingnetworks.pl;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import com.livingnetworks.demo2.R;

import android.app.Application;

@ReportsCrashes(
        formKey = "",
        formUri = "http://msl1.it.kth.se:5984/acra-mslapp/_design/acra-storage/_update/report",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        httpMethod = org.acra.sender.HttpSender.Method.PUT,
        formUriBasicAuthLogin = "dalek",
        formUriBasicAuthPassword = "doctor",
        // Your usual ACRA configuration
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text
        )
public class MSLApplication extends Application {
	  @Override
      public void onCreate() {
          super.onCreate();

          // The following line triggers the initialization of ACRA
          ACRA.init(this);
      }
}

package com.livingnetworks.pl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCPortOut;
import com.livingnetworks.demo2.R;

@SuppressLint("NewApi")
public class ShelfVisualSearch extends Activity implements View.OnClickListener{
	Context con;
	Activity acc;
	private FragmentManager fragmentManager;
	private Vibrator vibe;
	
	private OSCPortOut mSender;
	private OSCPortOut mSender_tv;
	
	private TextView list1;
	private TextView list2;
	private TextView list3;
	private boolean iflistPressed1 = false;
	private boolean iflistPressed2 = false;
	private boolean iflistPressed3 = false;
	private LinearLayout back;
	Typeface tf_regular;
	Typeface tf_boldItalic;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gantlondon_visualsearch);
		
		acc = this;
		con = this;
		fragmentManager  = this.getFragmentManager();
		vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		
		tf_regular = Typeface.createFromAsset(this.getAssets(), "AGaramondPro-Regular.otf");
		tf_boldItalic = Typeface.createFromAsset(this.getAssets(), "AGaramondPro-BoldItalic.otf");
		
		//TODO to request IP first; server_ip or server_ip_tv
		try {
			mSender = new OSCPortOut(InetAddress.getByName(MSLConfig.server_ip), MSLConfig.server_port);
		}catch (Exception e) {
			Toast.makeText(con, "network IP1", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		try {
			mSender_tv = new OSCPortOut(InetAddress.getByName(MSLConfig.server_ip_tv), MSLConfig.server_port);
		}catch (Exception e) {
			Toast.makeText(con, "network IP2", Toast.LENGTH_SHORT).show();
			e.printStackTrace();
		}
		
		
		back = (LinearLayout)findViewById(R.id.visualsearch_refer_middown);
		back.setOnClickListener(this);
		list1 = (TextView)findViewById(R.id.list1);
		list2 = (TextView)findViewById(R.id.list2);
		list3 = (TextView)findViewById(R.id.list3);
		list1.setOnClickListener(this);
		list2.setOnClickListener(this);
		list3.setOnClickListener(this);
		list1.setTextColor(Color.BLACK);
		list2.setTextColor(Color.BLACK);
		list3.setTextColor(Color.BLACK);
		
		list1.setTypeface(tf_regular);
		list2.setTypeface(tf_regular);
		list3.setTypeface(tf_regular);
		
	} // onCreate
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.list1:
			if(!iflistPressed1){
				oscSending("/list", 1);
				list1.setTypeface(tf_boldItalic, TRIM_MEMORY_BACKGROUND);
			}else{
				oscSending("/unlist", 1);
				list1.setTypeface(tf_regular, TRIM_MEMORY_BACKGROUND);
			}
			iflistPressed1 = !iflistPressed1;
			break;
		case R.id.list2:
			if(!iflistPressed2){
				oscSending("/list", 2);
				list2.setTypeface(tf_boldItalic, TRIM_MEMORY_BACKGROUND);
			}else{
				oscSending("/unlist", 2);
				list2.setTypeface(tf_regular, TRIM_MEMORY_BACKGROUND);
			}
			iflistPressed2 = !iflistPressed2;
			break;
		case R.id.list3:
			if(!iflistPressed3){
				oscSending("/list", 3);
				list3.setTypeface(tf_boldItalic,TRIM_MEMORY_BACKGROUND);
			}else{
				oscSending("/unlist", 3);
				list3.setTypeface(tf_regular, TRIM_MEMORY_BACKGROUND);
			}
			iflistPressed3 = !iflistPressed3;
			break;
		case R.id.visualsearch_refer_middown:
			finish(); //TODO
			break;
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		MSLConfig.removeNavBar(this);
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		MSLConfig.removeNavBar(this);
		super.onPause();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		oscSending("/stopvideo", 0);
		super.onDestroy();
	}
	
	//********************************
	public void oscSending(String address, int org) {
		if(mSender==null || mSender_tv==null){
			Toast.makeText(con, "network sender null", Toast.LENGTH_SHORT).show();
			return;
		}
			
		OSCMessage msg = new OSCMessage(address);
		msg.addArgument(org);
		try {
			mSender.send(msg);
			mSender_tv.send(msg);
			//Toast.makeText(con, address+","+org, Toast.LENGTH_SHORT).show();
			Log.v("shelf", "shelflist/"+address);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//********************************
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.layout.menu, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.action_edit:
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private boolean isWangluoKeyong() {
	    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
}
